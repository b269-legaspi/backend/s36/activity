/* ADDED CODES IN TASKROUTES AND TASKCONTROLLER FOR THE ACTIVITY
1. Create a route for getting a specific task.
2. Create a controller function for retrieving a specific task.
3. Return the result back to the client/Postman.
4. Process a GET request at the /tasks/:id route using postman to get a
specific task.
5. Create a route for changing the status of a task to complete.
6. Create a controller function for changing the status of a task to
complete.

Copyright@2019 Tuitt, Inc. and its affiliates. Confidential
Activity

6

7. Return the result back to the client/Postman.
8. Process a PUT request at the /tasks/:id/complete route using
postman to update a task.
9. Create a git repository named S36.
10. Initialize a local git repository, add the remote link and push to git with
the commit message of Add activity code.
11. Add the link in Boodle.

*/

// Setup the dependencies
const express = require("express");
const mongoose = require("mongoose");

// This allows us to use all the routes defined in "taskRoute.js"
const taskRoute = require("./routes/taskRoute");

// Server setup
const app = express();
const port = 3001;
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Database connection
// Connecting to MongoDB Atlas
mongoose.connect(
  "mongodb+srv://johnjeff01:admin123@zuitt-bootcamp.rctomsu.mongodb.net/s36?retryWrites=true&w=majority",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);
mongoose.connection.once("open", () =>
  console.log("Now connected to the database!")
);

// Add the task route
// Allows all the task routes created in the "taskRoute.js" file to use "/tasks" route
app.use("/tasks", taskRoute);

// Server listening
app.listen(port, () => console.log(`Now listening to port ${port}`));
